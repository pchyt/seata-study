package com.seata.apollo.stock.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.seata.apollo.stock.business.entity.Stock;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xwyang
 * @since 2020-12-15
 */
public interface IStockService extends IService<Stock> {

}
